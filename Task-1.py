#Підрахувати k - кількість цифр в десятковому запису цілого невід'ємного числа n.

n = int(input("Введи число: "))
k = int(0)

temp = n

while temp > 0:
    temp = temp // 10
    k += 1

print(f"У числа {n} - {k} цифри")